
function calculate(){
    let num1 = document.getElementById("num1").value;
    let operator = document.getElementById("operator").value;
    let num2 = document.getElementById("num2").value;

    let result;
    let expression;

    switch (operator) {
        case "*":
            result = parseInt(num1) * parseInt(num2);
            expression =  num1 +''+ '*' +''+ num2;
            break;
        case "/":
            result = parseInt(num1) / parseInt(num2);
            expression =  num1 +''+ '/' +''+ num2
            break;
        case "+":
            result = parseInt(num1) + parseInt(num2);
            expression =  num1 +''+ '+' +''+ num2
            break;
        case "-":
            result = parseInt(num1) - parseInt(num2);
            expression =  num1 +''+ '-' +''+ num2
            break;
        case "sqrt":
            result = parseInt(Math.sqrt(num1));
            expression =  '&#8730;(' + num1 + ')';
            break;
        case "pow":
            result = parseInt(Math.pow(num1, num2));
            expression =  num1 +''+ '^' +''+ num2;
            break;
    }

    console.log(expression);
    console.log(result);

    //print result
    document.getElementById("resultViewer").innerHTML = result;

    //add result to previous operations tab
    var btn = document.createElement("DIV");

    btn.innerHTML = '<h5><span id="lastOperation" class="badge badge-success">' + expression + '</span> =  <span id="lastOperation" class="badge badge-success">' + result + '</span></h5>';
    document.getElementById("previousOperations").appendChild(btn);


}

function usePreviousResultAsNum1(){
    let previousResult = document.getElementById("resultViewer").textContent;
    console.log('Previous Result = ' + previousResult);
    document.getElementById("num1").value = previousResult;
}

function usePreviousResultAsNum2(){
    let previousResult = document.getElementById("resultViewer").textContent;
    console.log('Previous Result = ' + previousResult);
    document.getElementById("num2").value = previousResult;
}
